# MailChimp API Connector

### Emocial Ruby on Rails Assignment

This program is available at [http://emocialassignment.herokuapp.com/]

This program has been created as a part of an assignment. It is a simple API
connector for MailChimp, which can fetch mailing lists and their subscribers.

Even though it's a simple Rails project, the source code has been carefully
documented to simplify reviewing.

## How it works

When you navigate to the root page, you will see a simple form where you are
asked to provide your MailChimp user name and the associated API key.

After clicking 'Connect', you will be taken to a page where your lists are
displayed. By clicking on a name of any list, a drop-down box will reveal the
emails of people subscribed to that list. On the right, you should see an
'Export' button, which has a drop-down menu for choosing the export format.

## Exporting a list

You can export a list in two different formats: CSV and JSON.

For simplicity, the CSV format contains a table with three columns:

- email address,
- first name,
- last name.

The JSON file contains the entire JSON object returned by MailChimp - it's
taken as-is and saved into a file.

Please note, that, even though the export functionalities are simple, they are
coded in such a way that it would be very easy to extend/modify them, as they
are based on the standard JSON and CSV Ruby classes for parsing the results.
