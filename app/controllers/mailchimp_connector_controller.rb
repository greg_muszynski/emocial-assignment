class MailchimpConnectorController < ApplicationController
  def index
  end

  # Populates the instance variables with lists and members, which are
  # immediately displayed in the view.
  #
  # The JSON class provides methods for turning a JSON object (represented as
  # a string) into a Ruby Hash object.
  def display_lists
    if params[:username].present? && params[:api_key].present?
      api_key_md = /^\h+-(?<data_centre>\w+)$/.match(params[:api_key])

      # Check if the :api_key parameter contains a well-formed MailChimp API key
      if api_key_md
        @username    = params[:username]
        @api_key     = api_key_md.to_s
        @data_centre = api_key_md[:data_centre]

        response = fetch_lists(@username, @api_key, @data_centre)
        if response.is_a? Net::HTTPSuccess
          response_json = JSON.parse(response.body)

          # This instance variable is used by the view to display lists by name
          @lists = response_json["lists"]

          # The members hash gets populated with members of each list.
          # A single entry is identified by its list_id, provided by MailChimp,
          # and contains an array of objects with members' details.
          @members = {}
          @lists.each do |list|
            list_id = list["id"]
            res = fetch_members(list_id, @username, @api_key, @data_centre)
            if res.is_a? Net::HTTPSuccess
              res_json = JSON.parse(res.body)
              @members[list_id] = res_json["members"]
            end
          end
        end
      else
        # Malformed API key
        redirect_to :index
      end
    else
      # One of the HTTP parameters is missing
      redirect_to :index
    end
  end


  # Sends a list of members to the browser using +send_data+
  #
  # The format of the file is specified by the `:format` parameter, which
  # can be JSON or CSV. By default (even when the format parameter is missing
  # from the request), the file is exported to CSV.
  # The CSV file contains less information compared to the JSON equivalent;
  # it is similar to the basic view of a list in the MailChimp's dashboard.
  # The JSON file, however, is a 'prettified' copy of the JSON object returned
  # by MailChimp API, containing all the details.
  def export_members
    if params[:list_id].present? && params[:username].present? &&
       params[:api_key].present? && params[:data_centre].present?

      res = fetch_members(params[:list_id], params[:username],
                          params[:api_key], params[:data_centre])

      if res.is_a? Net::HTTPSuccess
        data = JSON.parse(res.body)
        if params[:format].present? && params[:format] == "json"
          send_data JSON.pretty_generate(data), filename: "members.json"
        else
          # This branch is executed when the :format parameter is missing,
          # when it's set to anything, and in particular,
          # when it's set to "csv".
          require 'csv'
          csv_data = CSV.generate do |csv|
            csv << ["Email Address", "First Name", "Last Name"]
            data["members"].each do |member|
              csv << [member["email_address"],
                      member["merge_fields"]["FNAME"],
                      member["merge_fields"]["LNAME"]]
            end
          end
          send_data csv_data, filename: "members.csv"
        end
      end
    end
  end

private

  # Generalised method for sending API requests
  #
  # Returns an HTTP response object.
  #
  # This method uses built-in Ruby classes to connect to MailChimp API with SSL.
  # It is used by both +fetch_lists+ and +fetch_members+ and contains the code
  # common to both, while the difference is in the URLs only.
  def send_api_request(url, username, api_key)
    require 'net/https'
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_PEER
    request = Net::HTTP::Get.new(url.request_uri)
    request.basic_auth username, api_key
    http.request(request)
  end

  # Fetches users' MailChimp lists, by calling `GET /lists`
  def fetch_lists(username, api_key, data_centre)
    url = URI("https://#{data_centre}.api.mailchimp.com/3.0/lists")
    send_api_request(url, username, api_key)
  end

  # Fetches members of a list specified by `list_id`,
  # by calling `GET /lists/{list_id}/members`
  def fetch_members(list_id, username, api_key, data_centre)
    url = URI("https://#{data_centre}.api.mailchimp.com/3.0/lists/#{list_id}/members")
    send_api_request(url, username, api_key)
  end
end
